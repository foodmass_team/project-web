@extends("master") @section('title', 'Menu Restaurant') @section('content')
<div class="container">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-md-9 -->
        <div class="col-md-9">
            <div class="profile-box-left">
                <div class="profile-menu">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-edit" role="tablist">
                        <li role="presentation" class="active"><a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">Menu</a></li>
                        <li role="presentation"><a href="#openinghour" aria-controls="openinghour" role="tab" data-toggle="tab">Opening Hours</a></li>
                        <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Reviews</a></li>
                        <li role="presentation"><a href="#map" aria-controls="map" role="tab" data-toggle="tab">Map</a></li>
                        <li role="presentation"><a href="#bookatable" aria-controls="bookatable" role="tab" data-toggle="tab">Book and Table</a></li>
                        <li role="presentation"><a href="#photo" aria-controls="photo" role="tab" data-toggle="tab">Photo</a></li>
                        <li role="presentation"><a href="#information" aria-controls="information" role="tab" data-toggle="tab">Information</a></li>
                    </ul>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="menu">
                        <div class="bres-box-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <div class="list-group">
                                        <div class="list-border">
                                            <a href="#" class="list-group-item">
                                                <span class="badge"><span class="glyphicon glyphicon-chevron-right"></span></span>Cras justo odio
                                            </a>
                                            <a href="#" class="list-group-item">
                                                <span class="badge"><span class="glyphicon glyphicon-chevron-right"></span></span>
                                                Dapibus ac
                                            </a>
                                            <a href="#" class="list-group-item">
                                                <span class="badge"><span class="glyphicon glyphicon-chevron-right"></span></span>
                                                Morbi leo
                                            </a>
                                            <a href="#" class="list-group-item">
                                                <span class="badge"><span class="glyphicon glyphicon-chevron-right"></span></span>
                                                consectetur
                                            </a>
                                            <a href="#" class="list-group-item">
                                                <span class="badge"><span class="glyphicon glyphicon-chevron-right"></span></span>
                                                Vestibulum
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- close col-md-3 -->
                                <div class="col-md-9 col-sm-9">

                                    <!-- begin tab -->
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs tabs-edit" role="tablist" style="padding-bottom: 10px;">
                                            <li role="presentation" class="active"><a href="#list" class="btn btn-default" aria-controls="list" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list"></span></a></li>
                                            <li role="presentation"><a href="#grid" class="btn btn-default" aria-controls="grid" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-th-large"></span></a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <!-- list -->
                                            <div role="tabpanel" class="tab-pane active" id="list">
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="heading-border-bottom">
                                                            <div class="panel-heading" role="tab" id="headingOne">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Coffe New Delicuse
                                            </a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <div class="list-right">
                                                                    <a href="#" class="list-group-item" data-toggle="modal" data-target="#myDetail">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item" data-toggle="modal" data-target="#myDetail">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item" data-toggle="modal" data-target="#myDetail">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item" data-toggle="modal" data-target="#myDetail">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end panel-->
                                                <!--penel-->
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="heading-border-bottom">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" href="#collapse3">Collapsible list group</a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div id="collapse3" class="panel-collapse collapse in" role="tabpanel">
                                                            <div class="panel-body">
                                                                <div class="list-right">
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end panel-->
                                                <!-- panel-->
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="heading-border-bottom">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" href="#collapse4">Collapsible list group</a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <div id="collapse4" class="panel-collapse collapse in" role="tabpanel">
                                                            <div class="panel-body">
                                                                <div class="list-right">
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                    <a href="#" class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <img src="{{ url('img/06112016204756.png') }}" class="img-size">
                                                                            </div>
                                                                            <div class="col-md-7">
                                                                                <h4>
                                                                                    Homemade Nut Butter Selection | V |
                                                                                </h4>
                                                                                <p>
                                                                                    Slice of seeded toast. homemade almond butter and peanut butter.
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <p> Regular $ 2.75</p>
                                                                            </div>
                                                                        </div>

                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end panel-->
                                            </div>
                                            <!-- end list -->
                                            <!-- Grid -->
                                            <div role="tabpanel" class="tab-pane" id="grid">
                                                <div class="row">
                                                    <!-- box grid order col-4 -->
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="box-grid">
                                                            <div class="box-img-grid">
                                                                <img src="{{ url('img/burger1.jpg') }}" alt="" class="img-grid ">
                                                            </div>
                                                            <div class="box-title-grid">
                                                                <P>Grilled Beef Heart បេះដូងគោចង្កាក់ Grilled Beef Heart បេះដូងគោចង្កាក់ <br/><b>Price : </b>10$ </P>
                                                            </div>
                                                            <div class="box-btn-order">
                                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myDetail">Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end box grid order col-4 -->
                                                    <!-- box grid order col-4 -->
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="box-grid">
                                                            <div class="box-img-grid">
                                                                <img src="{{ url('img/burger1.jpg') }}" alt="" class="img-grid ">
                                                            </div>
                                                            <div class="box-title-grid">
                                                                <P>Grilled Beef Heart បេះដូងគោចង្កាក់ Grilled Beef Heart បេះដូងគោចង្កាក់ <br/><b>Price : </b>10$ </P>
                                                            </div>
                                                            <div class="box-btn-order">
                                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myDetail">Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end box grid order col-4 -->
                                                    <!-- box grid order col-4 -->
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="box-grid">
                                                            <div class="box-img-grid">
                                                                <img src="{{ url('img/burger1.jpg') }}" alt="" class="img-grid ">
                                                            </div>
                                                            <div class="box-title-grid">
                                                                <P>Grilled Beef Heart បេះដូងគោចង្កាក់ Grilled Beef Heart បេះដូងគោចង្កាក់ <br/><b>Price : </b>10$ </P>
                                                            </div>
                                                            <div class="box-btn-order">
                                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myDetail">Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end box grid order col-4 -->
                                                    <!-- box grid order col-4 -->
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="box-grid">
                                                            <div class="box-img-grid">
                                                                <img src="{{ url('img/burger1.jpg') }}" alt="" class="img-grid ">
                                                            </div>
                                                            <div class="box-title-grid">
                                                                <P>Grilled Beef Heart បេះដូងគោចង្កាក់ Grilled Beef Heart បេះដូងគោចង្កាក់ <br/><b>Price : </b>10$ </P>
                                                            </div>
                                                            <div class="box-btn-order">
                                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myDetail">Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end box grid order col-4 -->
                                                    <!-- box grid order col-4 -->
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="box-grid">
                                                            <div class="box-img-grid">
                                                                <img src="{{ url('img/burger1.jpg') }}" alt="" class="img-grid ">
                                                            </div>
                                                            <div class="box-title-grid">
                                                                <P>Grilled Beef Heart បេះដូងគោចង្កាក់ Grilled Beef Heart បេះដូងគោចង្កាក់ <br/><b>Price : </b>10$ </P>
                                                            </div>
                                                            <div class="box-btn-order">
                                                                <button type="button" class="btn btn-success">Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end box grid order col-4 -->
                                                    <!-- box grid order col-4 -->
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="box-grid">
                                                            <div class="box-img-grid">
                                                                <img src="{{ url('img/burger1.jpg') }}" alt="" class="img-grid ">
                                                            </div>
                                                            <div class="box-title-grid">
                                                                <P>Grilled Beef Heart បេះដូងគោចង្កាក់ Grilled Beef Heart បេះដូងគោចង្កាក់ <br/><b>Price : </b>10$ </P>
                                                            </div>
                                                            <div class="box-btn-order">
                                                                <button type="button" class="btn btn-success">Order</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end box grid order col-4 -->
                                                </div>
                                            </div>
                                            <!-- end Grid -->
                                        </div>
                                    </div>
                                    <!-- end tab -->

                                    <!--panel -->
                                </div>
                                <!-- close col-md-7 -->

                            </div>
                            <!--close row-->
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="openinghour">
                        <div class="bres-box-body">
                            <div class="box-opening">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Opening Hours -->
                                        <div class="title-in-browes-res">
                                            <div class="col-md-12 col-sm-12">
                                                <p>Opening Hours</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="opening-hours-box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Opening Hours
                                        </div>
                                        <div class="col-md-6">
                                            Opening Hours
                                        </div>
                                    </div>
                                </div>
                                <div class="opening-hours-box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Opening Hours
                                        </div>
                                        <div class="col-md-6">
                                            Opening Hours
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="reviews">
                        <div class="bres-box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="opening-button-right">
                                        <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#review" aria-expanded="false" aria-controls="collapseExample">WRITE A REVIEW</button>
                                        <div class="collapse" id="review" style="margin-top: 10px;">
                                            <div class="well">
                                                <form>
                                                    <div class="form-group">
                                                        <textarea rows="" cols=""></textarea>
                                                    </div>
                                                    <button type="submit" class="btn btn-warning">PUBLIST REVIEW</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-review">
                                        No reviews yet.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="map">
                        <div class="bres-box-body">
                            <div class="box-map-out">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-map">
                                            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.7742100463406!2d104.89248701480793!3d11.568037691787316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd09ff2f4d326e3f!2sSETEC+Institute!5e0!3m2!1sen!2s!4v1497164529235"
                                                frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="map-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-inline">
                                                <div class="form-group">
                                                    <input type="text" class="form-control no-border-radius" placeholder="Enter a Location">
                                                </div>
                                                <div class="form-group">
                                                    <select name="" class="form-control no-border-radius">
                                                                <option value="">Driving</option>
                                                                <option value="">Walking</option>
                                                                <option value="">Bicycling</option>
                                                                <option value="">Transit</option>
                                                            </select>
                                                </div>
                                                <button type="submit" class="btn btn-success">Get Directions</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="bookatable">
                        <div class="bres-box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-bookatable">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="title-in-browes-res">
                                                    <div class="col-md-12 col-sm-12">
                                                        <p>Book Information</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="line-in-book"></div>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Number Of Guests</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control no-border-radius">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Date Of Booking</label>
                                                <div class="col-md-3">
                                                    <input type="date" class="form-control no-border-radius">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Time</label>
                                                <div class="col-md-3">
                                                    <input type="time" class="form-control no-border-radius">
                                                </div>
                                            </div>
                                        </form>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="title-in-browes-res">
                                                    <div class="col-md-12 col-sm-12">
                                                        <p>Contact Information</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="line-in-book"></div>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control no-border-radius">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control no-border-radius">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Phone</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control no-border-radius">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Your Instructions</label>
                                                <div class="col-md-6">
                                                    <textarea class="form-control no-border-radius" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="photo">
                        <div class="bres-box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wra-box-photo">
                                        gallery not available
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="information">
                        <div class="bres-box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wra-box-info">
                                        No Information
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end Tab panes -->
            </div>
        </div>
        <!-- end col-md-9 -->

        <!-- being col-md-3 -->
        <div class="col-md-3">
            <div class="box-adv">
                <div class="box-adv-img">
                    <img src="{{ url('images/step3.png') }}" alt="..." class="img-responsive">
                </div>
                <div class="box-adv-img">
                    <img src="{{ url('images/step1.png') }}" alt="..." class="img-responsive">
                </div>
                <div class="box-adv-img">
                    <img src="{{ url('images/step4.png') }}" alt="..." class="img-responsive">
                </div>
            </div>
        </div>
        <!-- end col-md-3 -->

    </div>
    <!-- end row -->
</div>
@endsection