@extends("master") @section('title', 'Browes Restaurant') @section('content')
<div class="container">
    <div class="row">
        <!-- begin col-md-12 -->
        <div class="col-md-12">
            <div class="profile-box-left">
                <div class="profile-menu">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-edit" role="tablist">
                        <li role="presentation" class="active"><a href="#res-list" aria-controls="res-list" role="tab" data-toggle="tab">Restaurant List</a></li>
                        <li role="presentation"><a href="#newest" aria-controls="newest" role="tab" data-toggle="tab">Newest</a></li>
                        <li role="presentation"><a href="#featured" aria-controls="featured" role="tab" data-toggle="tab">Featured History</a></li>
                        <li role="presentation"><a href="#viewrestaurantmap" aria-controls="viewrestaurantmap" role="tab" data-toggle="tab">View Restaurant Map</a></li>
                    </ul>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="res-list">

                        <div class="wra-bro-res">
                            <div class="box-resss">
                                <div class="row">
                                    <!-- <div class=" col-sm-12 col-md-12"> -->
                                    <div class=" col-sm-6 col-md-6">
                                        <div class="boxtyra">
                                            <div class="row">
                                                <div class=" col-sm-3 col-md-3">
                                                    <img src="{{ url('img/master_gril.jpg') }}" width="100px" height="50%">
                                                    <div class="detail">
                                                        <ul class="ul-li">
                                                            <li>
                                                                Delivery
                                                                <i class="fa fa-check-circle-o far" aria-hidden="true"></i>
                                                            </li>
                                                            <li>
                                                                Pickup
                                                                <i class="fa fa-check-circle-o far" aria-hidden="true"></i>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <p class="text-available">Cash on delivery available</p>
                                                </div>
                                                <!--close col-3-->
                                                <div class=" col-sm-9 col-md-9">
                                                    <div class=" col-sm-4 col-md-4 star">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <!--close col-3-->
                                                    <div class=" col-sm-4 col-md-4">
                                                        <p>0 Reviews</p>
                                                    </div>
                                                    <!--close col-3-->
                                                    <div class=" col-sm-4 col-md-4">
                                                        <span class="label label-success">Open</span>
                                                    </div>
                                                    <!-- <div class="row"> -->
                                                    <div class=" col-sm-12 col-md-12">
                                                        <h3 class="shop-name">Master Suki Soup Bak Touk</h3>
                                                        <p>172, Tchecoslovaquie Blvd Phnom Penh Sangkat Boeng..</p>
                                                        <p class="food-type">Asian, Fried Rice, Khmer, Noodle, Soup / Phở</p>
                                                        <p>Minimum Order: -</p>
                                                        <p>Delivery Est: 45</p>
                                                        <p>Delivery Distance: 15</p>
                                                        <span class="label label-success">Free Delivery On Orders Over $ 15.00</span>
                                                    </div>
                                                    <div class=" col-sm-12 col-md-12 btn-view">
                                                        <input type="submit" value="View menu" class="btn btn-warning" style="font-size: 15px; width: 40%; margin-left: 35px; margin-top: 10px; ">
                                                    </div>
                                                    <!--close col-12-->
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <!--close col-9-->
                                        </div>
                                        <!--close back-->
                                    </div>
                                    <!--close col-6-->
                                    <!--close col-12-->
                                    <div class=" col-sm-6 col-md-6">
                                        <div class="boxtyra">
                                            <!-- <div class=" col-sm-12 col-md-12"> -->
                                            <iframe class="mapss" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.7742100463593!2d104.89248701435888!3d11.56803769178734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31095173761d4a53%3A0xcd09ff2f4d326e3f!2sSETEC+Institute!5e0!3m2!1sen!2skh!4v1501578464583"
                                                frameborder="0" style="border:0" allowfullscreen></iframe>
                                            <!-- </div> -->
                                            <!--close col-12-->
                                        </div>
                                        <!--close back-->
                                    </div>
                                    <!--close col-6-->
                                </div>
                            </div>
                            <!--close row-->
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="newest">

                        <div class="wra-bro-res">
                            <div class="box-resss">
                                <div class="row">
                                    <!-- <div class=" col-sm-12 col-md-12"> -->
                                    <div class=" col-sm-6 col-md-6">
                                        <div class="boxtyra">
                                            <div class="row">
                                                <div class=" col-sm-3 col-md-3">
                                                    <img src="{{ url('img/master_gril.jpg') }}" width="100px" height="50%">
                                                    <div class="detail">
                                                        <ul class="ul-li">
                                                            <li>
                                                                Delivery
                                                                <i class="fa fa-check-circle-o far" aria-hidden="true"></i>
                                                            </li>
                                                            <li>
                                                                Pickup
                                                                <i class="fa fa-check-circle-o far" aria-hidden="true"></i>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <p class="text-available">Cash on delivery available</p>
                                                </div>
                                                <!--close col-3-->
                                                <div class=" col-sm-9 col-md-9">
                                                    <div class=" col-sm-4 col-md-4 star">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <!--close col-3-->
                                                    <div class=" col-sm-4 col-md-4">
                                                        <p>0 Reviews</p>
                                                    </div>
                                                    <!--close col-3-->
                                                    <div class=" col-sm-4 col-md-4">
                                                        <span class="label label-success">Open</span>
                                                    </div>
                                                    <!-- <div class="row"> -->
                                                    <div class=" col-sm-12 col-md-12">
                                                        <h3 class="shop-name">Master Suki Soup Bak Touk</h3>
                                                        <p>172, Tchecoslovaquie Blvd Phnom Penh Sangkat Boeng..</p>
                                                        <p class="food-type">Asian, Fried Rice, Khmer, Noodle, Soup / Phở</p>
                                                        <p>Minimum Order: -</p>
                                                        <p>Delivery Est: 45</p>
                                                        <p>Delivery Distance: 15</p>
                                                        <span class="label label-success">Free Delivery On Orders Over $ 15.00</span>
                                                    </div>
                                                    <div class=" col-sm-12 col-md-12 btn-view">
                                                        <input type="submit" value="View menu" class="btn btn-warning" style="font-size: 15px; width: 40%; margin-left: 35px; margin-top: 10px; ">
                                                    </div>
                                                    <!--close col-12-->
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <!--close col-9-->
                                        </div>
                                        <!--close back-->
                                    </div>
                                    <!--close col-6-->
                                    <!--close col-12-->
                                    <div class=" col-sm-6 col-md-6">
                                        <div class="boxtyra">
                                            <!-- <div class=" col-sm-12 col-md-12"> -->
                                            <iframe class="mapss" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.7742100463593!2d104.89248701435888!3d11.56803769178734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31095173761d4a53%3A0xcd09ff2f4d326e3f!2sSETEC+Institute!5e0!3m2!1sen!2skh!4v1501578464583"
                                                frameborder="0" style="border:0" allowfullscreen></iframe>
                                            <!-- </div> -->
                                            <!--close col-12-->
                                        </div>
                                        <!--close back-->
                                    </div>
                                    <!--close col-6-->
                                </div>
                            </div>
                            <!--close row-->
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="featured">

                        <div class="wra-bro-res">
                            <div class="box-resss">
                                <div class="row">
                                    <!-- <div class=" col-sm-12 col-md-12"> -->
                                    <div class=" col-sm-6 col-md-6">
                                        <div class="boxtyra">
                                            <div class="row">
                                                <div class=" col-sm-3 col-md-3">
                                                    <img src="{{ url('img/master_gril.jpg') }}" width="100px" height="50%">
                                                    <div class="detail">
                                                        <ul class="ul-li">
                                                            <li>
                                                                Delivery
                                                                <i class="fa fa-check-circle-o far" aria-hidden="true"></i>
                                                            </li>
                                                            <li>
                                                                Pickup
                                                                <i class="fa fa-check-circle-o far" aria-hidden="true"></i>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <p class="text-available">Cash on delivery available</p>
                                                </div>
                                                <!--close col-3-->
                                                <div class=" col-sm-9 col-md-9">
                                                    <div class=" col-sm-4 col-md-4 star">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <!--close col-3-->
                                                    <div class=" col-sm-4 col-md-4">
                                                        <p>0 Reviews</p>
                                                    </div>
                                                    <!--close col-3-->
                                                    <div class=" col-sm-4 col-md-4">
                                                        <span class="label label-success">Open</span>
                                                    </div>
                                                    <!-- <div class="row"> -->
                                                    <div class=" col-sm-12 col-md-12">
                                                        <h3 class="shop-name">Master Suki Soup Bak Touk</h3>
                                                        <p>172, Tchecoslovaquie Blvd Phnom Penh Sangkat Boeng..</p>
                                                        <p class="food-type">Asian, Fried Rice, Khmer, Noodle, Soup / Phở</p>
                                                        <p>Minimum Order: -</p>
                                                        <p>Delivery Est: 45</p>
                                                        <p>Delivery Distance: 15</p>
                                                        <span class="label label-success">Free Delivery On Orders Over $ 15.00</span>
                                                    </div>
                                                    <div class=" col-sm-12 col-md-12 btn-view">
                                                        <input type="submit" value="View menu" class="btn btn-warning" style="font-size: 15px; width: 40%; margin-left: 35px; margin-top: 10px; ">
                                                    </div>
                                                    <!--close col-12-->
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <!--close col-9-->
                                        </div>
                                        <!--close back-->
                                    </div>
                                    <!--close col-6-->
                                    <!--close col-12-->
                                    <div class=" col-sm-6 col-md-6">
                                        <div class="boxtyra">
                                            <!-- <div class=" col-sm-12 col-md-12"> -->
                                            <iframe class="mapss" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.7742100463593!2d104.89248701435888!3d11.56803769178734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31095173761d4a53%3A0xcd09ff2f4d326e3f!2sSETEC+Institute!5e0!3m2!1sen!2skh!4v1501578464583"
                                                frameborder="0" style="border:0" allowfullscreen></iframe>
                                            <!-- </div> -->
                                            <!--close col-12-->
                                        </div>
                                        <!--close back-->
                                    </div>
                                    <!--close col-6-->
                                </div>
                            </div>
                            <!--close row-->
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="viewrestaurantmap">

                        <div class="wra-bro-res">
                            <div class="box-resss">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box-map">
                                            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.7742100463406!2d104.89248701480793!3d11.568037691787316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd09ff2f4d326e3f!2sSETEC+Institute!5e0!3m2!1sen!2s!4v1497164529235"
                                                frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- end col-md-12 -->
    </div>
</div>
@endsection