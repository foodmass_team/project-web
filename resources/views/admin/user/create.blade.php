@extends("admin.master")
 @section('title', 'User') @section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif 
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form" action="{{ url('/system/user') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input -->
                    <div class="form-group">
                        <label>Role</label> 
                            <select class="form-control" name="role">
                              @foreach($list_role as $item)
                                <option value='{{$item->RoleID}}'>{{$item->RoleName}}</option> 
                            @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="Username" value="{{old('Username')}}" class="form-control" placeholder="Enter your Username">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="Password" class="form-control" placeholder="Enter your password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="Confirmpassword" class="form-control" placeholder="Enter your confirm password">
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection