@extends('admin.master') 
@section('title','Staff')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i>Staff</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/staff/create') }}" role="button">Create New Staff</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th> 
                        <th>Staff Name</th>
                        <th>Gender</th>
                        <th>DOB</th>
                        <th>Address</th>
                        <th>Tel</th>
                        <th>Email</th>
                        <th>Salary</th>
                        <th>Position</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
              
                    @foreach($list_staff as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td> 
                            <td>{{$item->SName}}</td>
                            <td>{{$item->Gender}}</td>
                            <td>{{$item->DOB}}</td>
                            <td>{{$item->Address}}</td>
                            <td>{{$item->Tel}}</td>
                            <td>{{$item->Email}}</td>
                            <td>{{$item->Salary}}</td>
                            <td>{{$item->Position}}</td>
                            <td>
                                <a href="#"><i class="fa fa-lock"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection