@extends("admin.master") @section('title', 'Staff') @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif 
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add Staff</h3>
            </div>
            <div class="box-body">
                <form role="form" action="{{ url('/system/staff') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input -->
                    <div class="row"> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Staff Name</label>
                                <input type="text" name="SName" value="{{old('SName')}}" class="form-control" placeholder="Enter your staff name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Gender</label>
                                <select class="form-control" name="Gender">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>DOB</label>
                                <input type="date" name="DOB" value="{{old('DOB')}}" class="form-control" placeholder="Enter your DOB">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="Address" value="{{old('Address')}}" class="form-control" placeholder="Enter your Address">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tel</label>
                                <input type="text" name="Tel" class="form-control" value="{{old('Tel')}}" placeholder="Enter your Tel">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="Email" class="form-control" placeholder="Enter your email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Salary</label>
                                <input type="text" name="Salary" value="{{old('Salary')}}" class="form-control" placeholder="Enter your salary">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Position</label>
                                <input type="text" name="Position" value="{{old('Positio')}}" class="form-control" placeholder="Enter your position">
                            </div>
                        </div>
                    </div>
                   
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endsection