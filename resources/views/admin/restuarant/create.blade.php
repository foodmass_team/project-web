@extends("admin.master") @section('title', 'Restuarant') @section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add Restuarant</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form" action="{{ url('/system/restuarant') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Restuarant ID</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Restuarant ID">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Restuarant Name</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Restuarant Name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Address">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Restuarant Tel</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Restuarant Tel">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Restuarant Email</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Restuarant Email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Contact Name</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Contact Name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Position</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Position">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Contact Tel</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Contact Tel">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Contact Email</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your Contact Email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>OpenTime</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your OpenTime">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>CloseTime</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your CloseTime">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>StatusA</label>
                                <select class="form-control" name="role">
                                    <option value="">--select--</option>
                                    <option value="open">Open</option>
                                    <option value="close">Close</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>StatusB</label>
                                <select class="form-control" name="role">
                                    <option value="">--select--</option>
                                    <option value="request">Request</option>
                                    <option value="approved">Approved</option>
                                    <option value="block">Block</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>RegisterDateTime</label>
                                <input type="Date" name="" class="form-control" placeholder="Enter your RegisterDateTime">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>SID</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your SID">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>CuisineID</label>
                                <input type="text" name="" class="form-control" placeholder="Enter your CuisineID">
                            </div>
                        </div>


                        {{--  <div class="col-md-4">
                            <div class="form-group">
                                <label>Gender</label>
                                <select class="form-control" name="role">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>  --}}
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection