@extends('admin.master') 

@section('title','Supplier')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i>Supplier</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/supplier/create') }}" role="button">Create New Supplier</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th> 
                        <th>Supplier Name</th>
                        <th>Contact Name</th> 
                        <th>Address</th>
                        <th>Tel</th>
                        <th>Email</th> 
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($list_supplier as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td> 
                        <td>{{$item->SupName}}</td> 
                        <td>{{$item->ContactName}}</td>
                        <td>{{$item->Address}}</td>
                        <td>{{$item->Tel}}</td>
                        <td>{{$item->Email}}</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection