@extends("admin.master") @section('title', 'Supplier') @section('content')

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif 
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add Supplier</h3>
            </div>
            <!-- /.box-header -->
            {{--  SupID, SupName, ContactName, Address, Tel, Email)  --}}
            <div class="box-body">
                <form role="form" action="{{ url('/system/supplier') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input --> 
                    <div class="form-group">
                        <label>Supplier Name</label>
                        <input type="text" name="SupName" class="form-control" value="{{old('SupName')}}" placeholder="Enter Supplier Name">
                    </div>
                    <div class="form-group">
                        <label>Contact Name </label>
                        <input type="text" name="ContactName" class="form-control" value="{{old('ContactName')}}" placeholder="Enter your Contact Name,">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="Address" class="form-control"  value="{{old('Address')}}" placeholder="Enter your Address">
                    </div>
                    <div class="form-group">
                        <label>Telephone</label>
                        <input type="number" name="Tel" class="form-control" placeholder="Enter your Telephone">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="Email" class="form-control" placeholder="Enter your Email">
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection