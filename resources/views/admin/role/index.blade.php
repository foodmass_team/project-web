@extends('admin.master')

@section('title','Role')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Role</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/role/create') }}" role="button">Create New Role</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th> 
                        <th>Role Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($list_role as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td> 
                        <td>{{$item->RoleName}}</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection