@extends('admin.master')

@section('title','role')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif 
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Add Role</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ url('/system/role') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- text input --> 
                        <div class="form-group">
                            <label>Role Name</label>
                            <input type="text" name="RoleName" class="form-control" placeholder="Enter your Role name">
                        </div>
                        <div class="box-footer">
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection