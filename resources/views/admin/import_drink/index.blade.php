@extends('admin.master')

@section('title','Category')

@section('content')

	<div class="row"> 
	    <div class="col-md-12 col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-users"></i> Import Drink</h3>
                    <a class="btn btn-primary pull-right" href="{{ url('/system/import_drink/create') }}" role="button">Create New Drink</a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="app-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Import Date</th>
                                <th>Total Amount</th>
                                <th>Staff Name</th>
                                <th>Supplier Name</th>
                                <th>Detial</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($list_import as $item)
                            <tr> 
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->ImportDate}}</td>
                                <td>{{$item->TAmount}}</td></td>
                                <td>{{$item->SID}}</td>
                                <td>{{$item->SupID}}</td>
                                <td>
                                    <a href="#"  class="btn btn-success" data-toggle="modal" data-target="#myModal-{{$item->ImportID}}" style="text-decoration:underline;">View Detial</i></a> |
                                    <a href="{{ url('/system/import_drink/create') }}"  class="btn btn-success" data-toggle="modal" data-target="#myEdit" style="text-decoration:underline;">Update</i></a>
                                </td>
                            </tr>
                            <div class="modal fade modal-Defualt" id="myModal-{{$item->ImportID}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"> View Import Drink {{$item->ImportID}}</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="#" method="POST">
                                                    {{ csrf_field() }}
                                                        <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-md-12 col-ms-12 col-xs-12" >
                                                                <div style="border:1px solid black; overflow: scroll;overflow-x: hidden;  height: 299px;">
                                                                     
                                                                    <div class="row" style="border-bottom:1px solid black;">
                                                                         <div class="col-md-2 col-sm-2">
                                                                            <h4 class=" pull-left">No</h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2">
                                                                             <h4 class=" pull-left">Drink </h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2">
                                                                              <h4 class=" pull-left">Type</h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2">
                                                                              <h4 class=" pull-left">Qty</h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2">
                                                                            <h4 class=" pull-left">Cost</h4>
                                                                        </div>
                                                                        <div class="col-md-2 col-sm-2">
                                                                            <h4 class=" pull-left">Amount</h4>
                                                                        </div>

                                                                    </div>
                                                                    <div class="row" style="border-bottom:1px solid red;">
                                                                        @foreach($list_import_detail as $item1)
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <h5 class=" pull-left">{{$item1->importID}}</h5>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <h5 class=" pull-left">{{$item1->importID}} </h5>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <h5 class=" pull-left">{{$item1->importID}}</h5>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <h5 class=" pull-left">{{$item1->importID}}</h5>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <h5 class=" pull-left">{{$item1->importID}}</h5>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <h5 class=" pull-left">{{$item1->importID}}</h5>
                                                                            </div>
                                                                        @endforeach
                                                                    </div> 
                                                                    
                                                                </div>
                                                                <div class="pull-right">
                                                                    {{-- <input style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default" type="submit" value="Procces to check out" style=""> --}}
                                                                    <a href="{{ url('system/import_drink') }}" style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default">OK</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal fade modal-defualt" id="myDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content" style="    margin-top: 77px;">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Import Detail</h4>
                                            </div>
                                            <div class="modal-footer" style="text-align: left !important;">
                                                <form action="" method="POST" enctype="multipart/form-data">
                                                    
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-md-12 col-ms-12 col-xs-12">
                                                                <div class="" style="overflow: scroll;  overflow-x: hidden;  height: 299px;">
                                                                    <table class="table table-condensed">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>Drink Name</th>
                                                                                <th>Qty</th>
                                                                                <th> Type</th>
                                                                                <th> Cost</th>
                                                                                <th> Total Amount</th>
                                                                            
                                                                            </tr>

                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td> 1</td>
                                                                                <td> Coca Cola</td>
                                                                                
                                                                                <td> Case</td>
                                                                                <td> 10</td>
                                                                                <td> 10</td>
                                                                                <td> 100$</td>
                                                                            </tr>
                                                                        
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="pull-right">
                                                                    {{-- <input style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default" type="submit" value="Procces to check out" style=""> --}}
                                                                    <a href="{{ url('system/import_drink') }}" style="margin-top: 10px; background: #5e48d4; color:white;" class="btn btn-default">OK</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-body -->
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            </div>
        </div>
    </div>

@endsection