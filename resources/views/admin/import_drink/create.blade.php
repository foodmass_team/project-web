@extends("admin.master") @section('title', 'Import Drink')
@section('content')
<div class="row">
    <div class="col-md-4 col-sm-4">
        <div class="box box-primary">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif 
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user">Import Drink</i></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                     <div class="form-group">
                        <label>Supplier Name</label>
                        <select class="form-control" id="SupName">
                            @foreach($list_supplier as $item)
                                <option value='{{$item->SupID.'!'.$item->SupName}}'>{{$item->SupName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Drink Name</label>
                        <select class="form-control" id="Drink">
                            @foreach($list_drink as $item)
                                <option value='{{$item->DID.'!'.$item->DName}}'>{{$item->DName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty </label>
                        <input type="number" id="Qty" class="form-control" placeholder="Qty">
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" id="Type">
                            <option value="case">Case</option>
                            <option value="Unit">Unit</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Cost</label>
                        <input type="number" id="Cost" class="form-control" placeholder="Cost">
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Add" id="btn_add" class="btn btn-primary pull-right">
                    </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-8 col-sm-8">
          <div class="box">
            <div class="box-body">
                <table class="table table-bordered" id="app-table">
                    <thead>
                        <tr>
                            <th style="display:none;">id</th>
                            <th>#</th> 
                            <th>Drink</th>
                            <th>Type</th>
                            <th>Qty</th>
                            <th>Cost</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                   
                </table>
            </div>
             <div class="box-footer">
                        <input type="button" value="Save" Name="bnt_save" class="btn btn-primary pull-right">
             </div>
        </div>
    </div>
</div>

@endsection
@section('Jquery')
 <script>
		
		var index=1;
		var update=false;
		var cmdUpdate;
		var trUpdate;
		$('#Drink').focus();
		$("#btn_add").click(function(){
			if(this.value=="Add"){
				var oTable = document.getElementById('app-table');
				var rowLength = oTable.rows.length;
				var name=document.getElementById('Drink').value.split('!')[1];
				var DID=document.getElementById('Drink').value.split('!')[0];
				var qty=document.getElementById('Qty').value;
				var price=document.getElementById('Cost').value;
				var type=document.getElementById('Type').value;
                var amount = price*qty;
                if(price==""){
                        alert("please input Cost !!!");
                        return false;
                }
                else if(qty==""){
                    alert("Please Input Qty !!!")
                    return false;
                }
			   $("#app-table").append("<tr id='"+(index)+"'><td>"+rowLength+"</td><td style='display:none'>"+DID+"</td><td>"+name+"</td><td>"+qty+"</td><td>"+price+"</td><td>"+type+"</td><td>"+amount+"</td><td><button style='margin-right:10%;' onclick='updates("+index+", this)'><i class='fa fa-edit'></i></button><button  onclick='deletes("+index+")'><i class='fa fa-trash'></i></button></td></tr>");
			   index++;
			   sum();
			   clearText();
			}else{
				trUpdate.item(1).innerHTML=$('#Drink').val().split('!')[0];
				trUpdate.item(2).innerHTML=$('#Drink').val().split('!')[1];
				trUpdate.item(3).innerHTML=$('#Qty').val();
				trUpdate.item(4).innerHTML=$('#Cost').val();
				trUpdate.item(5).innerHTML=$('#Type').val();
                trUpdate.item(6).innerHTML=parseFloat($('#Cost').val())*parseInt($('#Qty').val());
				sum();
				clearText();
				cmdUpdate.innerHTML="Update";
				this.value="Add";
				update=false;
			}
			$('#Drink').focus();
		});
		$("#btn_save").click(function(){
			var oTable = document.getElementById('app-table');
			var rowLength = oTable.rows.length;   
			if(rowLength==1){
				alert("Not found data to import");
			}else{
				for (i = 1; i < rowLength; i++){
					var oCells = oTable.rows.item(i).cells;
					var st="";
					var DID= oCells.item(1).innerHTML
					var name= oCells.item(2).innerHTML
					var qty= oCells.item(3).innerHTML
					var price= oCells.item(4).innerHTML
					var type= oCells.item(5).innerHTML
					alert("Drink ID : "+DID+"   Name : "+name+"  Qty : "+qty+"  Price : "+price+"   Type : "+type);
				}
			}
		});
		function sum(){
			var total=0;
			var totalQty=0;
			var oTable = document.getElementById('app-table');
			var rowLength = oTable.rows.length;   
			for (i = 1; i < rowLength; i++){
				var oCells = oTable.rows.item(i).cells;
				var price = oCells.item(4).innerHTML
				var qty = oCells.item(3).innerHTML
				total+=parseFloat(price);
				totalQty+=parseInt(qty);
			}
			$("#total").html("$ "+ total.toLocaleString());
			$("#qty").html(totalQty);
			setNo();
		}
	function deletes(id){
		$("#" + id).remove();
		$('#btn_add').val("Add");
		update=false;
		if(cmdUpdate!=null){
			cmdUpdate.innerHTML="Update";
		}
		clearText();
		sum();
	}
	function updates(id, cmd){
		if(!update){
			$('#app-table tr').each(function() {
				var trID=this.id;
				if(trID==id){
					var col = this.cells;
					document.getElementById('Drink').value=col.item(1).innerHTML+"!"+col.item(2).innerHTML;
					$('Qty').val(col.item(3).innerHTML)
					$('#Cost').val(col.item(4).innerHTML)
					$('#Type').val(col.item(5).innerHTML)
					$('#btn_add').val("Update");
					cmd.innerHTML="Cancel";
					update=true;
					trUpdate=this.cells;
					cmdUpdate=cmd;
					$('#Drink').focus();
					return false;
				}

			})
		}else if(cmd.innerHTML=="Cancel"){
			$('#btn_add').val("Add");
			update=false;
			cmd.innerHTML="Update";
			clearText();
		}
	}
	function clearText(){
		$('#Qty').val("");
		$('#Cost').val("");
	}
	function setNo(){
		var i=0;
		$('#app-table tr').each(function(){
			if(i!=0)
				this.cells.item(0).innerHTML=i;
			i++;
		});
	}
</script>
@endsection