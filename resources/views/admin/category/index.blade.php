@extends('admin.master')

@section('title','Category')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Category</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/category/create') }}" role="button">Create New Category</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>  
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($list_category as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->CatName}}</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection