@extends('admin.master')

@section('title','SubModule')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> SubModule</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/submodule/create') }}" role="button">Create New SubModule</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th> 
                        <th>Module Name</th>
                        <th>SubModule Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($list_submodule as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td> 
                        <td>{{$item->MName}}</td>
                        <td>{{$item->SMName}}</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection