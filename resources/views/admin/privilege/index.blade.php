@extends('admin.master')

@section('title','Privilege')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i> Privilege</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/privilege/create') }}" role="button">Create New Privilege</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Role</th>
                        <th>Module</th>
                        <th>Sub Module</th>
                        <th>Privilege</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($list_privilage as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->RoleName}}</td>
                        <td>{{$item->MName}}</td>
                        <td>{{$item->SMName}}</td>
                        <td>{{$item->Privilage}}</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection