@extends("admin.master") @section('title', 'Privilege') @section('content')

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
          @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif 
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add Privilege</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form role="form" action="{{ url('/system/privilege') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input -->
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role">
                            @foreach($list_role as $item_role)
                                <option value='{{$item_role->RoleID}}'>{{$item_role->RoleName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Module</label>
                        <select class="form-control" name="Module">
                            @foreach($list_module as $item_module)
                                <option value='{{$item_module->MID}}'>{{$item_module->MName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>SubModule</label>
                        <select class="form-control" name="SubModule">
                            @foreach($list as $item_sub_module)
                                <option value='{{$item_sub_module->SMID}}'>{{$item_sub_module->SMName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Privilege</label>
                        <input type="text" name="Privilage" class="form-control" placeholder="Enter your Privilege">
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection