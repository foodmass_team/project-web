<!-- Sidebar Menu -->
<ul class="sidebar-menu">
    <li class="header">HEADER</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="{{ url('system/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li><a href="{{ url('system/restuarant') }}"><i class="fa fa-dashboard"></i> <span>Restaurant Register</span></a></li>
    <li class="treeview">
        <a href="#"><i class="fa fa-user"></i> <span>Staff  Managerment</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ url('/system/staff') }}"><i class="fa fa-circle-o"></i> Staff</a></li>
            <li><a href="{{ url('/system/user') }}"><i class="fa fa-circle-o"></i> Users</a></li>
            <li><a href="{{ url('/system/role') }}"><i class="fa fa-circle-o"></i> Role</a></li>
            <li><a href="{{ url('/system/module') }}"><i class="fa fa-circle-o"></i> Module</a></li>
            <li><a href="{{ url('/system/submodule') }}"><i class="fa fa-circle-o"></i> SubModule</a></li>
            <li><a href="{{ url('/system/privilege') }}"><i class="fa fa-circle-o"></i> Privilege</a></li>
        </ul>
    </li>
     <li class="treeview">
        <a href="#"><i class="fa fa-user"></i> <span>Import Drink</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
           <li><a href="{{ url('/system/import_drink') }}"><i class="fa fa-circle-o"></i> Import Drink</a></li>
            <li><a href="{{ url('/system/supplier') }}"><i class="fa fa-circle-o"></i> Supplier</a></li>
            <li><a href="{{ url('/system/category') }}"><i class="fa fa-circle-o"></i> Category</a></li> 
            <li><a href="{{ url('/system/drink') }}"><i class="fa fa-circle-o"></i> Drink</a></li> 
        </ul>
    </li>
</ul>
<!-- /.sidebar-menu -->