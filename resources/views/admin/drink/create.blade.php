@extends("admin.master") @section('title', 'Drink') @section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif 
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Add Drink</h3>
            </div>
             {{--  // DName, Size, QtyCase, TypeCase, QtyUnit, UnitPrice, TypeUnit, CAvgCost, UAvgCost, CatID)  --}}
            <!-- /.box-header -->
            
            <div class="box-body">
                <form role="form" action="{{ url('/system/drink') }}" method="POST">
                    {{ csrf_field() }}
                    <!-- text input --> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Drink Name</label>
                                <input type="text" name="DName" class="form-control" placeholder="Enter your drink name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Size</label>
                                <select class="form-control" name="Size">
                                    <option value="Small">Small</option>
                                    <option value="Mediem">Mediem</option>
                                    <option value="Large">Large</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Qty Case</label>
                                <input type="number" name="QtyCase" class="form-control" placeholder="Enter your Qty Case">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Type Case</label>
                                <input type="text" name="TypeCase" class="form-control" placeholder="Enter your Type Case">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Qty Unit</label>
                                <input type="number" name="QtyUnit" class="form-control" placeholder="Enter your Qty Unit">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Unit Price</label>
                                <input type="number" name="UnitPrice" class="form-control" placeholder="Enter your Unit Price">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Type Unit</label>
                                <input type="text" name="TypeUnit" class="form-control" placeholder="Enter your Type Unit">
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label>Category Name</label>
                                <select class="form-control" name="category">
                                    @foreach($list_category as $item)
                                        <option value='{{$item->CatID}}'>{{$item->CatName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" value="Save" class="btn btn-primary pull-right">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

@endsection