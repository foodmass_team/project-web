@extends('admin.master') 

@section('title','Drink')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-users"></i>Drink</h3>
            <a class="btn btn-primary pull-right" href="{{ url('/system/drink/create') }}" role="button">Create New Drink</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id="app-table">
                <thead>
                    <tr>
                        <th>#</th> 
                        <th>Name</th>
                        <th>Size</th>
                        <th>Qty Case</th>
                        <th>Type Case</th>
                        <th>Qty Unit</th>
                        <th>Unit Price</th>
                        <th>Type Unit</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($list_drink as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td> 
                        <td>{{$item->DName}}</td>
                        <td>{{$item->Size}}</td>
                        <td>{{$item->QtyCase}}</td>
                        <td>{{$item->TypeCase}}</td>
                        <td>{{$item->QtyUnit}}</td>
                        <td>{{$item->UnitPrice}}</td>
                        <td>{{$item->TypeUnit}}</td>
                        <td>{{$item->CatName}}</td>
                        <td>
                            <a href="#"><i class="fa fa-lock"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection