@extends("master") 
@section('title', 'Home') 
@section('content')

<!-- Promotion -->
<div style="box-shadow: 0 4px 8px 0px #F2B410;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="w3ls-title">Promotion</h3>
                <div class="hid-space"></div>
                <div class="col-md-3">
                    <img src="{{ url('images\step1.png') }}" style="width:100%;height:200px;">
                    <h2 class="space-promo"> Partner1</h2>
                </div>
                <div class="col-md-3">
                    <img src="{{ url('images\step2.png') }}" style="width:100%;height:200px;">
                    <h2 class="space-promo"> Partner 2</h2>
                </div>
                <div class="col-md-3">
                    <img src="{{ url('images\step3.png') }}" style="width:100%;height:200px;">
                    <h2 class="space-promo"> Partner 3</h2>
                </div>
                <div class="col-md-3">
                    <img src="{{ url('images\step4.png') }}" style="width:100%;height:200px;">
                    <h2 class="space-promo"> Partner 4</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Promotion -->
<div class="hid-space"></div>
<div class="container">
    <!--begin row-->
    <div class="row">
        <div class="col-md-12">
            <div class="hid-space"></div>
            <h3 class="w3ls-title">Featured Restaurants</h3>
            <div class="hid-space"></div>
        </div>
        <div class="col-md-6">
            <!--begin menu-wrapper-->
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('img/1462761924-12801653_927662034021951_6019720063226350017_n.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price ">Close</p>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                                <!--end menu-description-->
                            </a>
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('img/1462761924-12801653_927662034021951_6019720063226350017_n.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                                <!--end menu-description-->
                            </a>
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('img/1462761924-12801653_927662034021951_6019720063226350017_n.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                                <!--end menu-description-->
                            </a>
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('images/Add_to_Cart-512.png') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                                <!--end menu-description-->
                            </a>
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('images/1490512465-Logo1.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                            </a>
                            <!--end menu-description-->
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('images/1490512465-Logo1.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                            </a>
                            <!--end menu-description-->
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('images/1490512465-Logo1.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                            </a>
                            <!--end menu-description-->
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-6">
            <figure class="effect-ruby">
                <!-- <img src="img/13.jpg" alt="img13"/> -->
                <figcaption>
                    <!--begin menu-wrapper-->
                    <div class="bor-style">
                        <div class="menu-wrapper">
                            <a href="{{ url('/home/menu') }}">
                                <!--begin menu-image-->
                                <div class="menu-image">
                                    <img src="{{ url('images/1490512465-Logo1.jpg') }}" class="width-100" alt="food">
                                </div>
                                <!--end menu-image-->
                                <!--begin menu-description-->
                                <div class="menu-description">
                                    <!--begin menu-list-->
                                    <div class="menu-list">
                                        <h5>English Breakfast</h5>
                                        <p class="price">$12.95</p>
                                        <span class="menu-dot-line"></span>
                                    </div>
                                    <!--end menu-list-->
                                    <p class="menu-ingredients">Scrambled eggs with crispy bacon, sausage, fresh black pudding, breadcumbs, onion, mustard, canbbery nut, baked brie, rucola, tomato &amp; mushroom.</p>
                                </div>
                            </a>
                            <!--end menu-description-->
                        </div>
                        <!--end menu-wrapper-->
                    </div>
                </figcaption>
            </figure>
        </div>
    </div>
    <!--end row-->
</div>
<!--end container-->

@endsection