<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    protected $table = 'tbl_drink';
    protected $primarykey = 'DID';
    protected $fillable = [
        'DName','Size','QtyCase','TypeCase','QtyUnit','UnitPrice','TypeUnit'
    ]; 
}
