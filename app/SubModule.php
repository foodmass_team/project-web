<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModule extends Model
{
    protected $table = 'tbl_sub_module';
    protected $primarykey = 'SMID';
    protected $fillable = [
        'SMName'
    ]; 
}
