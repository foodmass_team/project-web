<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilage extends Model
{
    protected $table = 'tbl_privilage';
    
    protected $fillable = [
        'Privilage'
    ]; 
}
