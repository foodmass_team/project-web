<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{ 
    // use Notifiable;
    protected $table = 'tbl_role';
    protected $primarykey = 'RoleID';
    protected $fillable = [
        'RoleName',
    ]; 
}
