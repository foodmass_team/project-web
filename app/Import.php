<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $table = 'tbl_import';
    protected $primarykey = 'ImportID';
    protected $fillable = [
        'ImportDate','TAmount','SID','SupID'
    ];
}
