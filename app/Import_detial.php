<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import_detial extends Model
{
    protected $table = 'tbl_import_detail';
    protected $primarykey = 'ImportDID';
    protected $fillable = [
        'Qty','Type','QtyCase','Cost'
    ];
}
