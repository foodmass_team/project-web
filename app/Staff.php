<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    // (SID, SName, Gender, DOB, Address, Tel, Email, Salary, Position)
    protected $table = 'tbl_staff_manager';
    protected $primarykey = 'SID';
    protected $fillable = [
        'SName','Gender','DOB','Address','Tel','Email','Salary','Position'
    ]; 
}
