<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VarSizeController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.varsize.index');
    }
    public function create()
    {
        return View('adminrestuarant.varsize.create');
    }
    public function store()
    {
        return redirect('/systemres/varsize');
    }
}
