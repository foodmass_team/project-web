<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MenuTypeController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.menutype.index');
    }
    public function create()
    {
        return View('adminrestuarant.menutype.create');
    }
    public function store()
    {
        return redirect('/systemres/menutype');
    }
}
