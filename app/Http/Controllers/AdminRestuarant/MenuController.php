<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.menu.index');
    }
    public function create()
    {
        return View('adminrestuarant.menu.create');
    }
    public function store()
    {
        return redirect('/systemres/menu');
    }
}
