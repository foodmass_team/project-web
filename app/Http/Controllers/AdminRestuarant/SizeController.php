<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.size.index');
    }
    public function create()
    {
        return View('adminrestuarant.size.create');
    }
    public function store()
    {
        return redirect('/systemres/size');
    }
}
