<?php

namespace App\Http\Controllers\AdminRestuarant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VarOptionController extends Controller
{
    public function Index()
    {
        return View('adminrestuarant.varoption.index');
    }
    public function create()
    {
        return View('adminrestuarant.varoption.create');
    }
    public function store()
    {
        return redirect('/systemres/varoption');
    }
}
