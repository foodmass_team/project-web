<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BrowseRestaurantController extends Controller
{
    public function Index(){
        return View('browse_restaurant.index');
    }
}
