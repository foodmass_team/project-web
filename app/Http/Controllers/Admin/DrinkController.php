<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Drink;
use DB;
use App\Category;
class DrinkController extends Controller
{
    public function Index(){
        // $users = DB::table('users')
        // ->join('contacts', 'users.id', '=', 'contacts.user_id')
        // ->join('orders', 'users.id', '=', 'orders.user_id')
        // ->select('users.*', 'contacts.phone', 'orders.price')
        // ->get();
        $list_drink = DB::table('tbl_drink')
            ->join('tbl_category','tbl_category.CatID','=','tbl_drink.CatID')
            ->select('tbl_drink.*','tbl_category.CatName')
            ->get();
        $data = array(
            "list_drink" =>$list_drink
        );
        return View('admin.drink.index',$data);
    }
    public function create(){
        $list_category = Category::all();
        
         $data = array(
             "list_category" =>$list_category
         );

        return View('admin.drink.create',$data);
    }
    public function store(Request $request){
         // DName, Size, QtyCase, TypeCase, QtyUnit, UnitPrice, TypeUnit, CAvgCost, UAvgCost, CatID)
        $this->validate($request,[
            'DName'=>'required',
            'Size'=>'required',
            'QtyCase'=>'required',
            'TypeCase'=>'required',
            'QtyUnit'=>'required',
            'UnitPrice'=>'required',
            'TypeUnit'=>'required' 
        ]);
        $Drink = new Drink;
        $Drink->DName=$request->DName;
        $Drink->Size=$request->Size;
        $Drink->QtyCase=$request->QtyCase;
        $Drink->TypeCase=$request->TypeCase;
        $Drink->QtyUnit=$request->QtyUnit;
        $Drink->UnitPrice=$request->UnitPrice;
        $Drink->TypeUnit=$request->TypeUnit;
        $Drink->CAvgCost=1;
        $Drink->UAvgCost=1;
        $Drink->CatID=$request->category;
        $Drink->save();
        return redirect('/system/drink');
    }
}
