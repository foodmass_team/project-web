<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Module;
class ModuleController extends Controller
{
    public function Index(){
        $list_module = Module::all();
        $data = array(
            "list_module" =>$list_module
        );
        return View('admin.module.index',$data);
    }
    public function create(){
        return View('admin.module.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'MName'=>'required|unique:tbl_module'
        ]);
        $module = new Module;
        $module->MName=$request->MName;
        $module->save();
        return redirect('/system/module');
    }
}
