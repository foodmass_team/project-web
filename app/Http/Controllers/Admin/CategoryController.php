<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    public function Index(){
        $list_category = Category::all();
        
         $data = array(
             "list_category" =>$list_category
         );

        return View('admin.category.index',$data);
    }
    public function create(){
        return View('admin.category.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'CatName'=>'required|unique:tbl_category',
        ]); 
        $Category = new Category;
        $Category->CatName=$request->CatName;
        $Category->save(); 
        return redirect('/system/category');
    }
}
