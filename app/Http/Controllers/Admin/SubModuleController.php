<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SubModule;
use App\Module;
use DB;
class SubModuleController extends Controller
{
    public function Index(){
        $list_submodule = DB::table('tbl_sub_module')
        ->join('tbl_module','tbl_module.MID','=','tbl_sub_module.MID')
        ->select('tbl_sub_module.*','tbl_module.MName')
        ->get();
        $data = array(
        "list_submodule" =>$list_submodule
    );
    //     $list_submodule = SubModule::all();
    //    $data = array('list_submodule'=>$list_submodule);
        return View('admin.submodule.index',$data);
    }
    public function create(){
        $list_module = Module::all();
        $data = array(
            "list_module" =>$list_module
        );
        return View('admin.submodule.create',$data);
    }
    public function store(Request $request){
        $this->validate($request,[
            'SMName'=>'required'
        ]);
        $Submodule= new SubModule;
        $Submodule->SMName=$request->SMName;
        $Submodule->MID=$request->MName;
        $Submodule->save();
        return redirect('/system/submodule');
    }
}
