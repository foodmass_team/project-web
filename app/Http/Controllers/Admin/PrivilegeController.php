<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Privilage;
use App\Role;
use App\Module;
use App\SubModule;
use DB;
class PrivilegeController extends Controller
{
    public function Index(){
        $list_privilage = DB::table('tbl_privilage')
        ->join('tbl_role','tbl_role.RoleID','=','tbl_privilage.RoleID')
        ->join('tbl_module','tbl_module.MID','=','tbl_privilage.MID')
        ->join('tbl_sub_module','tbl_sub_module.SMID','=','tbl_privilage.SMID')
        ->select('tbl_privilage.*','tbl_module.MName','tbl_role.RoleName','tbl_sub_module.SMName')
        ->get();
        $data = array(
        "list_privilage" =>$list_privilage
    );
        // $list_privilage = Privilage::all();
        // $data = array(
        //     'list_privilage'=>$list_privilage
        // );
        return View('admin.privilege.index',$data);
    }
    public function create(){
// module
        $list_module = Module::all();
        $data ["list_module"]= $list_module;
//sub module
        $list = SubModule::all();
        $data["list"]=$list;

// role
        $list_role = Role::all();
         $data["list_role"] =$list_role;
//  dd($data);
        return View('admin.privilege.create', $data );
    }
    public function store(Request $request){
        $this->validate($request,[
           'Privilage'=>'required'
        ]);
        $Privilage = new Privilage;
        $Privilage->RoleID=$request->role;
        $Privilage->MID=$request->Module;
        $Privilage->SMID=$request->SubModule;
        $Privilage->Privilage=$request->Privilage;
        $Privilage->save();
        return redirect('/system/privilege');
    }
}
