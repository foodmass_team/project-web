<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// use  App\Classes\RoleController;
use App\User;
use App\Role;
use DB;
class UserController extends RoleController
{
    public function Index()
    {
        $list_user = DB::table('users')
        ->join('tbl_role','tbl_role.RoleID','=','users.RoleID')
        ->select('users.*','tbl_role.RoleName','tbl_role.RoleID')
        ->get();
        $data = array(
        "list_user" =>$list_user
    );
        // $list_user = User::all();
       
        // $data = array(
        //     "list_user" =>$list_user
        // );
        return View('admin.user.index',$data);
    }
    public function create()
    {
        
    //    $Role = new RoleController;
    //     $data1 =array($Role->index());
        $list_role = Role::all();
        
        $data = array(
            "list_role" =>$list_role
        );
        return View('admin.user.create',$data);
    }
    public function store(Request $request)
    {   
      $this->validate($request,[
            'Username'=>'required|unique:users',
            'Password'=>'required|min:2|max:8',
            'Confirmpassword'=>'required|min:2|max:8|same:Password'
        ]); 
        $User = new User;
        $User->RoleID=$request->role;
        $User->Username=$request->Username;
        $User->Password=Hash::make($request->Password);
        $User->save(); 
        return redirect('/system/user');
    }
}
