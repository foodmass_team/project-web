<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Role;
class RoleController extends Controller
{
    public function Index(){

        $list_role = Role::all();
       
        $data = array(
            "list_role" =>$list_role
        );
    //   return  View('admin.user.create',$data);
        return View('admin.role.index',$data);
    }
    public function create(){
        return View('admin.role.create');
    }
    public function store(Request $request){

        $this->validate($request,[
            'RoleName'=>'required|unique:tbl_role'
        ]);
        $role = new Role;
        $role->RoleName=$request->RoleName;
        $role->save();
        return redirect('/system/role'); 
    }
}
