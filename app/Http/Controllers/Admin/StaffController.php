<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Staff;
use DB;
class StaffController extends Controller
{
    public function Index(){
        $list_staff = Staff::all();
         $data = array(
             "list_staff" =>$list_staff
         );
        return View('admin.staff.index',$data);
    }
    public function create(){
        return View('admin.staff.create');
    }
    public function store(Request $request){
        // (SID, SName, Gender, DOB, Address, Tel, Email, Salary, Position)
        $this->validate($request,[
            'SName'=>'required|unique:tbl_staff_manager',
            'Gender'=>'required',
            'DOB'=>'required',
            'Address'=>'required',
            'Tel'=>'required',
            'Email'=>'required|unique:tbl_staff_manager',
            'Salary'=>'required',
         
        ]);
        $Staff = new Staff;
        $Staff->SName=$request->SName;
        $Staff->Gender=$request->Gender;
        $Staff->DOB=$request->DOB;
        $Staff->Address=$request->Address;
        $Staff->Tel=$request->Tel;
        $Staff->Email=$request->Email;
        $Staff->Salary=$request->Salary;
        $Staff->Position=$request->Position;
        $Staff->save();
        return redirect('/system/staff');
    }
}
