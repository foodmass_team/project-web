<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Drink;
use App\Import_detial;
use App\Import;
use App\Supplier;
class ImportDrinkController extends Controller
{
    public function Index(){
        $list_import = DB::table('tbl_import_detail')
        ->join('tbl_import','tbl_import.ImportID','=','tbl_import_detail.ImportID')
        ->select('tbl_import_detail.*','tbl_import.*')
        ->get();
    $data ["list_import"] = $list_import;


    
        $list_import_detail = Import_detial::all();
        // ->where('importID', '=', $id);
        $data ["list_import_detail"]=$list_import_detail; 

        return View('admin.import_drink.index',$data);
    }
    public function create(){
        // Drink
        $list_drink = Drink::all();
        $data ["list_drink"]=$list_drink; 
        // Supplier
        $list_supplier = Supplier::all();
        $data ["list_supplier"]=$list_supplier; 
       
        return View('admin.import_drink.create',$data);
    }
    public function store(Request $request){
       
      
        $this->validate($request,[
            'Drink'=>'required',
            'Qty'=>'required',
            'Type'=>'required',
            'Cost'=>'required',
        ]);
        // Import (ImportID, ImportDate, TAmount, SID, SupID)
        
        $Import = new Import;
        $Import->TAmount=$request->Qty * $request->Cost;
        $Import->SID=1;
        $Import->SupID=$request->SupName;
        $Import->save();
        $list_import_id = DB::table('tbl_import')
        ->select(('ImportID'))
        ->get()->last();
        $data = $list_import_id;

        $Import_detial = new Import_detial;
        $Import_detial->importID=$list_import_id->ImportID;
        $Import_detial->Qty=$request->Qty;
        $Import_detial->Type=$request->Type;
        $Import_detial->Cost=$request->Cost;
        $Import_detial->DID=$request->Drink;
        $Import_detial->Total=$request->Qty*$request->Cost;
        $Import_detial->save();
        return redirect('/system/import_drink');
    }
}
