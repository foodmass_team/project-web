<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Supplier;
class SupplierController extends Controller
{
    public function Index(){
        $list_supplier = Supplier::all();
        $data = array(
            "list_supplier" =>$list_supplier
        );
        return View('admin.supplier.index',$data);
    }
    public function create(){
        return View('admin.supplier.create');
    }
    public function store(Request $request){
        $this->validate($request,[
            'SupName'=>'required|unique:tbl_supplier',
            'ContactName'=>'required',
            'Address'=>'required',
            'Tel'=>'required',
            'Email'=>'required|email' 
        ]);
        $Supplier = new Supplier;
        $Supplier->SupName=$request->SupName;
        $Supplier->ContactName=$request->ContactName;
        $Supplier->Address=$request->Address;
        $Supplier->Tel=$request->Tel;
        $Supplier->Email=$request->Email;
        $Supplier->save();
        return redirect('/system/supplier');
    }
}
