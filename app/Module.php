<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'tbl_module';
    protected $primarykey = 'MID';
    protected $fillable = [
        'MName',
    ]; 
}
