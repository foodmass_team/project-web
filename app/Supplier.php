<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    // SupID, SupName, ContactName, Address, Tel, Email)
    protected $table = 'tbl_supplier';
    protected $primarykey = 'SupID';
    protected $fillable = [
        'SupName','ContactName','Address','Tel','Email'
    ]; 
}
